package pl.wikibuster.bookcrossing.network.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;
import pl.wikibuster.bookcrossing.network.model.BookUser;
import pl.wikibuster.bookcrossing.network.model.User;

/**
 * Created by kuba on 24.10.2015.
 */
public class BorrowingUserRequest extends RetrofitSpiceRequest<BookUser, BookCrossingApi> {


    private long id;

    public BorrowingUserRequest(long id) {
        super(BookUser.class, BookCrossingApi.class);
        this.id = id;
    }

    @Override
    public BookUser loadDataFromNetwork() throws Exception {
        return getService().getRequestUser(id);
    }
}

package pl.wikibuster.bookcrossing.network.model;

/**
 * Created by kuba on 24.10.2015.
 */
public class Token {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

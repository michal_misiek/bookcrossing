package pl.wikibuster.bookcrossing.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import pl.wikibuster.bookcrossing.MainActivity;
import pl.wikibuster.bookcrossing.Message;
import pl.wikibuster.bookcrossing.R;
import pl.wikibuster.bookcrossing.network.LoginRetrofitSpiceService;
import pl.wikibuster.bookcrossing.network.model.BookUser;
import pl.wikibuster.bookcrossing.network.model.Token;
import pl.wikibuster.bookcrossing.network.request.AuthenticationRequest;
import pl.wikibuster.bookcrossing.network.request.BookUserRequest;

public class LoginActivity extends Activity {

    public static final String PREF_TOKEN = "pref_token";
    public static final String TOKEN = "token";
    public static final String USER = "user";

    private EditText username;
    private EditText password;

    private SpiceManager spiceManager = new SpiceManager(LoginRetrofitSpiceService.class);

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String token = getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE).getString(TOKEN, "empty");
        Log.i("Tag", token);
        if (!token.equals("empty")) {
            startApp();
            return;
        }

        setContentView(R.layout.activity_login);


        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
    }

    public void login(View view) {
        AuthenticationRequest request = new AuthenticationRequest(username.getText().toString(), password.getText().toString());
        spiceManager.execute(request, new RequestListener<Token>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.e("Login", "Login unsuccessful", spiceException);
                Message.message(getBaseContext(), "Login failed");
            }

            @Override
            public void onRequestSuccess(Token token) {
                Log.i("Login", "token = " + token.getToken());
                SharedPreferences preferences = getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
                preferences.edit()
                        .putString(TOKEN, token.getToken())
                        .apply();

                BookUserRequest bookUserRequest = new BookUserRequest("Token " + token.getToken());
                spiceManager.execute(bookUserRequest, new RequestListener<BookUser>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        Log.e("Login", "Login unsuccessful", spiceException);
                        Message.message(getBaseContext(), "Login failed");
                    }

                    @Override
                    public void onRequestSuccess(BookUser bookUser) {
                        Log.i("Login", "User id = " + bookUser.getId());
                        SharedPreferences preferences = getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
                        preferences.edit()
                                .putLong(USER, bookUser.getId())
                                .apply();

                        startApp();
                    }
                });
            }
        });
    }

    private void startApp() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}

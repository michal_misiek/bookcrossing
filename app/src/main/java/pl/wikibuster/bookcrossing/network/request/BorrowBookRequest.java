package pl.wikibuster.bookcrossing.network.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import greendao.BookDb;
import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;

/**
 * Created by kuba on 25.10.2015.
 */
public class BorrowBookRequest extends RetrofitSpiceRequest<Book, BookCrossingApi> {

    private final BookDb book;

    public BorrowBookRequest(BookDb book) {
        super(Book.class, BookCrossingApi.class);
        this.book = book;
    }

    @Override
    public Book loadDataFromNetwork() throws Exception {
        return getService().borrow(book.getServerId());
    }
}

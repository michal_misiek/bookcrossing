package pl.wikibuster.bookcrossing.network.model;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import greendao.RawBookDb;

/**
 * Created by kuba on 24.10.2015.
 */
public class RawBook {

    private long id;
    private String isbn_10;
    private String isbn_13;
    private String title;
    private List<Author> authors;
    private String publisher;
    private int year;
    private List<String> genre;
    private float rationg;
    private String thumbnail;
    private String language;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsbn_10() {
        return isbn_10;
    }

    public void setIsbn_10(String isbn_10) {
        this.isbn_10 = isbn_10;
    }

    public String getIsbn_13() {
        return isbn_13;
    }

    public void setIsbn_13(String isbn_13) {
        this.isbn_13 = isbn_13;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public float getRationg() {
        return rationg;
    }

    public void setRationg(float rationg) {
        this.rationg = rationg;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public RawBookDb toDb() {
        return new RawBookDb(null, id, isbn_10, isbn_13, title, StringUtils.join(authors, ", "), publisher, year, StringUtils.join(genre, ", "), thumbnail, language, rationg);
    }
}

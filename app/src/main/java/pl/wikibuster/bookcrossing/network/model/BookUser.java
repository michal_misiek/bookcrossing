package pl.wikibuster.bookcrossing.network.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by kuba on 24.10.2015.
 */
public class BookUser {

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    private long id;
    private String contact_info;
    private String full_name;
    private float rating;
    private String last_modification;
    private long user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContactInfo() {
        return contact_info;
    }

    public void setContactInfo(String contactInfo) {
        this.contact_info = contactInfo;
    }

    public String getFullName() {
        return full_name;
    }

    public void setFullName(String full_name) {
        this.full_name = full_name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getLastModification() {
        return last_modification;
    }

    public void setLastModification(String lastModification) {
        this.last_modification = lastModification;
    }

    public long getUser() {
        return user;
    }

    public void setUser(long user) {
        this.user = user;
    }
}

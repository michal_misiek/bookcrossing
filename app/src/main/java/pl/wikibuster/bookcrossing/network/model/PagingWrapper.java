package pl.wikibuster.bookcrossing.network.model;

import java.util.List;

/**
 * Created by kuba on 24.10.2015.
 */
public class PagingWrapper<T> {

    private long count;
    private String next;
    private String previous;
    private List<T> results;

    public long getCount() {
        return count;
    }

    public String getNext() {
        return next;
    }

    public String getPrevious() {
        return previous;
    }

    public List<T> getResults() {
        return results;
    }
}

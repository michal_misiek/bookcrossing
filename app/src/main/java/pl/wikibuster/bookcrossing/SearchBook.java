package pl.wikibuster.bookcrossing;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.Arrays;
import java.util.List;

import pl.wikibuster.bookcrossing.library.LibraryAdapter;
import pl.wikibuster.bookcrossing.network.request.BookSearchRequest;

//TODO checks subtitles
public class SearchBook extends BaseActivity {

    private static final String TAG = "SearchBook";

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LibraryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Log.d(TAG, "Handle intent");
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            View view = this.getCurrentFocus();
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d(TAG, "query = " + query);
            BookSearchRequest request = new BookSearchRequest(query);
            getSpiceManager().execute(request, new SearchBookListener());
        }
    }

    @Override
    public boolean onSearchRequested() {
        Log.d(TAG, "onSearchRequested");
        Bundle appData = new Bundle();
        startSearch(null, false, appData, false);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        searchView.setQueryHint(getString(R.string.searchHint));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchItem.collapseActionView();
                Intent searchIntent = new Intent(getApplicationContext(), SearchBook.class);
                searchIntent.putExtra(SearchManager.QUERY, s);
                searchIntent.setAction(Intent.ACTION_SEARCH);
                startActivity(searchIntent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private class SearchBookListener implements RequestListener<FullBook[]> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e(TAG, "search failed", spiceException);
        }

        @Override
        public void onRequestSuccess(FullBook[] fullBooks) {
            List<FullBook> books = Arrays.asList(fullBooks);

            adapter = new LibraryAdapter(books, getSpiceManager());
            recyclerView.setAdapter(adapter);
        }
    }
}

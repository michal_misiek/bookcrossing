package pl.wikibuster.bookcrossing.network;

import pl.wikibuster.bookcrossing.network.model.Book;
import pl.wikibuster.bookcrossing.network.model.BookUser;
import pl.wikibuster.bookcrossing.network.model.LoginUser;
import pl.wikibuster.bookcrossing.network.model.PagingWrapper;
import pl.wikibuster.bookcrossing.network.model.RawBook;
import pl.wikibuster.bookcrossing.network.model.Token;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by kuba on 24.10.2015.
 */
public interface BookCrossingApi {

    @GET("/search/{isbn}")
    RawBook search(@Path("isbn") String isbn);

    @GET("/books/")
    PagingWrapper<Book> getBooksByOwner(@Query("user") long owner);

    @GET("/books/")
    PagingWrapper<Book> getBooksByOwner(@Query("user") long owner, @Query("page") int page);

    @GET("/books/")
    PagingWrapper<Book> getBooksBorrowed(@Query("borrowed_by") long user);

    @GET("/books/")
    PagingWrapper<Book> getBooksBorrowed(@Query("borrowed_by") long user, @Query("page") int page);

    @GET("/books/")
    PagingWrapper<Book> searchBooks(@Query("query") String search);

    @GET("/books/")
    PagingWrapper<Book> searchBooks(@Query("query") String search, @Query("page") int page);

    @GET("/books/")
    PagingWrapper<Book> getBooks(@Query("limit") int limit);

    @GET("/books/{bookId}")
    Book getBook(@Path("id") long bookId);



    @GET("/raw-books/{id}")
    RawBook getRawBook(@Path("id") long id);

    @POST("/book/{raw_book_id}")
    Book addBook(@Path("raw_book_id") long rawBookId);

    @POST("/token-auth/")
    Token authenticate(@Body LoginUser user);

    @GET("/user/")
    BookUser getUser(@Header("Authorization") String authentication);

    @GET("/user/")
    BookUser getUser();

    @GET("/book-users/{id}")
    BookUser getRequestUser(@Path("id") long userId);

    @POST("/borrow/{id}")
    Book borrow(@Path("id") long bookId);
}

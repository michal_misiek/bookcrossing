package pl.wikibuster.bookcrossing;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.Arrays;
import java.util.List;

import greendao.BookDb;
import greendao.BookDbDao;
import greendao.DaoMaster;
import greendao.DaoSession;
import greendao.RawBookDb;
import greendao.RawBookDbDao;
import pl.wikibuster.bookcrossing.library.LibraryAdapter;
import pl.wikibuster.bookcrossing.login.LoginActivity;
import pl.wikibuster.bookcrossing.network.model.BookUser;
import pl.wikibuster.bookcrossing.network.request.UsersBooksRequest;

public class History extends BaseActivity {
    public static final String PREFS_MODIFICATION_DATE = "last_modification_date";
    public static final String MODIFICATION_DATE = "modification_date";
    public static final String TAG = "Library";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LibraryAdapter adapter;
    List<BookDb> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        displayBooks();

        String token = getSharedPreferences(LoginActivity.PREF_TOKEN, Context.MODE_PRIVATE).getString(LoginActivity.TOKEN, "");
        //BookUserRequest bookUserRequest = new BookUserRequest(token);
        //getSpiceManager().execute(bookUserRequest, new BookUserListener());

    }

    private void downloadBooks(BookUser bookUser) {
        UsersBooksRequest booksRequest = new UsersBooksRequest(bookUser);
        getSpiceManager().execute(booksRequest, new RequestListener<FullBook[]>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.w(TAG, "API communication failed", spiceException);
            }

            @Override
            public void onRequestSuccess(FullBook[] fullBooks) {
                List<FullBook> books = Arrays.asList(fullBooks);
                DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(History.this, getString(R.string.databaseName), null);
                SQLiteDatabase db = helper.getWritableDatabase();
                DaoMaster daoMaster = new DaoMaster(db);
                DaoSession daoSession = daoMaster.newSession();
                RawBookDbDao rawBookDbDao = daoSession.getRawBookDbDao();
                List<RawBookDb> rawBooksDb = FullBook.getRawBooksDb(fullBooks);
                List<BookDb> booksDb = FullBook.getBooksDb(fullBooks);
                rawBookDbDao.insertInTx(rawBooksDb);

                for (int i = 0; i < rawBooksDb.size(); i++) {
                    booksDb.get(i).setRawBookDb(rawBooksDb.get(i));
                }

                BookDbDao bookDbDao = daoSession.getBookDbDao();
                bookDbDao.insertInTx(booksDb);

                adapter = new LibraryAdapter(books, getSpiceManager());
                recyclerView.setAdapter(adapter);
            }
        });
    }

    private void displayBooks() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(History.this, getString(R.string.databaseName), null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        BookDbDao bookDbDao = daoSession.getBookDbDao();
        try {
            data = bookDbDao.queryBuilder().list();

            adapter = new LibraryAdapter(FullBook.of(data), getSpiceManager());
            recyclerView.setAdapter(adapter);
        } catch (Exception e) {
            LayoutInflater layoutInflater = LayoutInflater.from(History.this);
            View view = layoutInflater.inflate(R.layout.nothing_to_show, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(History.this);
            alertDialogBuilder.setView(view);
            alertDialogBuilder.setNeutralButton((getString(R.string.goBack)),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });

            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
    }
/*
    private class BookUserListener implements RequestListener<BookUser> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.w(TAG, "API communication failed", spiceException);
            Message.message(History.this, "Sorry, wrong ISBN!");
        }

        @Override
        public void onRequestSuccess(BookUser bookUser) {
            try {
                long externalTime = BookUser.DATE_FORMAT.parse(bookUser.getLastModification()).getTime();
                SharedPreferences sharedPreferences = History.this.getSharedPreferences(PREFS_MODIFICATION_DATE, Context.MODE_PRIVATE);
                long localTime = sharedPreferences.getLong(MODIFICATION_DATE, 0);
                if (localTime < externalTime) {
                    Log.i(TAG, "External time is newer, downloading books");
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putLong(MODIFICATION_DATE, externalTime);
                    editor.apply();
                    downloadBooks(bookUser);
                    Message.message(History.this, "OK, book added!");
                } else {
                    Log.d(TAG, "No need to download new books");
                }
            } catch (ParseException e) {
                Log.e(TAG, "could not parse " + bookUser.getLastModification(), e);
            }

        }
    }
*/
}

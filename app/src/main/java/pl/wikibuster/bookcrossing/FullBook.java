package pl.wikibuster.bookcrossing;

import java.util.ArrayList;
import java.util.List;

import greendao.BookDb;
import greendao.RawBookDb;

/**
 * Created by kuba on 24.10.2015.
 */
public class FullBook {

    private final BookDb bookDb;
    private final RawBookDb rawBookDb;

    public FullBook(BookDb bookDb, RawBookDb rawBookDb) {
        this.bookDb = bookDb;
        this.rawBookDb = rawBookDb;
    }

    public FullBook(BookDb bookDb) {
        this.bookDb = bookDb;
        this.rawBookDb = bookDb.getRawBookDb();
    }

    public BookDb getBookDb() {
        return bookDb;
    }

    public RawBookDb getRawBookDb() {
        return rawBookDb;
    }

    public static List<BookDb> getBooksDb(FullBook[] fullBooks) {
        List<BookDb> list = new ArrayList<>();
        for (FullBook fullBook : fullBooks) {
            list.add(fullBook.getBookDb());
        }
        return list;
    }

    public static List<RawBookDb> getRawBooksDb(FullBook[] fullBooks) {
        List<RawBookDb> list = new ArrayList<>();
        for (FullBook fullBook : fullBooks) {
            list.add(fullBook.getRawBookDb());
        }
        return list;
    }

    public static List<FullBook> of(List<BookDb> books) {
        List<FullBook> list = new ArrayList<>();
        for (BookDb book : books) {
            list.add(new FullBook(book));
        }
        return list;
    }
}

package pl.wikibuster.bookcrossing.library;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.List;

import greendao.RawBookDb;
import pl.wikibuster.bookcrossing.FullBook;
import pl.wikibuster.bookcrossing.Message;
import pl.wikibuster.bookcrossing.R;
import pl.wikibuster.bookcrossing.login.LoginActivity;
import pl.wikibuster.bookcrossing.network.model.Book;
import pl.wikibuster.bookcrossing.network.request.BorrowBookRequest;

/**
 * Created by kuba on 24.10.2015.
 */
public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.ViewHolder> {

    private final List<FullBook> data;
    private final SpiceManager spiceManager;
    private Context context;
    private static long user = 1L;

    public LibraryAdapter(List<FullBook> data, SpiceManager spiceManager) {
        this.data = data;
        this.spiceManager = spiceManager;
    }

    public void add(FullBook fullBook) {
        data.add(fullBook);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.library_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        user = context.getSharedPreferences(LoginActivity.PREF_TOKEN, Context.MODE_PRIVATE).getLong(LoginActivity.USER, 1L);

        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        FullBook book = data.get(position);
        RawBookDb rawBook = book.getRawBookDb();
        final Long borrowedBy = book.getBookDb().getBorrowedBy();

        if (rawBook != null) {
            holder.title.setText(rawBook.getTitle());
            holder.author.setText(rawBook.getAuthor());
            if(borrowedBy != null){
                holder.linearLayout.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.pink));
            } else {
                holder.linearLayout.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.white));
            }
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final FullBook fullBook = data.get(position);
                    LayoutInflater layoutInflater = LayoutInflater.from(context);
                    View promptView = layoutInflater.inflate(R.layout.book_show, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setView(promptView);

                    final TextView bookText = (TextView) promptView.findViewById(R.id.bookText);
                    bookText.setText(fullBook.getRawBookDb().getFullText() + fullBook.getBookDb().getFullText() + ((borrowedBy != null) ? ("\nstatus: borrowed") : "" ) + ((user == fullBook.getBookDb().getServerOwnerId()) ? ("\nowner: you") : "" ));
                    if (( borrowedBy != null || user == fullBook.getBookDb().getServerOwnerId() )) {
                        alertDialogBuilder.setNeutralButton(context.getString(R.string.OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //TODO żeby tu zawsze wiedziało jaki jest użytkownik
                                        dialog.cancel();
                                    }
                                });
                    } else if(borrowedBy == null && user == fullBook.getBookDb().getServerOwnerId()) {
                        alertDialogBuilder.setPositiveButton(context.getString(R.string.lend),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //TODO żeby tu zawsze wiedziało jaki jest użytkownik
                                        dialog.cancel();
                                    }
                                })
                                .setNegativeButton(context.getString(R.string.cancel),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                    }
                    else{
                            alertDialogBuilder.setPositiveButton(context.getString(R.string.borrow),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            BorrowBookRequest request = new BorrowBookRequest(fullBook.getBookDb());
                                            spiceManager.execute(request, new RequestListener<Book>() {
                                                @Override
                                                public void onRequestFailure(SpiceException spiceException) {
                                                    Log.e("LibraryAdapter", "Could not borrow book", spiceException);
                                                    Message.message(context, "Could not borrow book");
                                                }

                                                @Override
                                                public void onRequestSuccess(Book book) {
                                                    Message.message(context, "Owner of the book was informed about your request");
                                                }
                                            });
                                            dialog.cancel();
                                        }
                                    })
                                    .setNegativeButton(context.getString(R.string.cancel),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                    }
                    AlertDialog alert = alertDialogBuilder.create();
                    alert.show();
                }
            });
        } else {
            Log.w("Library", "rawBook is null!");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView author;
        public final LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            author = (TextView) itemView.findViewById(R.id.author);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.layout);
        }
    }

}

package pl.wikibuster.bookcrossing.network.model;

import greendao.BookDb;

/**
 * Created by kuba on 24.10.2015.
 */
public class Book {

    private Long id;
    private float rating;
    private String description;
    private long raw_book;
    private Long user;
    private Long borrowed_by;
    private String location;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getRaw_book() {
        return raw_book;
    }

    public void setRaw_book(long raw_book) {
        this.raw_book = raw_book;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getBorrowed_by() {
        return borrowed_by;
    }

    public void setBorrowed_by(Long borrowed_by) {
        this.borrowed_by = borrowed_by;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public BookDb toDb() {
        return new BookDb(null, id, null, user, rating, borrowed_by, location, description);
    }
}

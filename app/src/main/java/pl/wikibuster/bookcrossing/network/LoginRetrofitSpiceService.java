package pl.wikibuster.bookcrossing.network;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

/**
 * Created by kuba on 24.10.2015.
 */
public class LoginRetrofitSpiceService extends RetrofitGsonSpiceService {

    private static final String BASE_URL = "http://wikibuster.pl/api";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(BookCrossingApi.class);
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }
}

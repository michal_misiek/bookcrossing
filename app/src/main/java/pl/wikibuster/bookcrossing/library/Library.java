package pl.wikibuster.bookcrossing.library;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import greendao.BookDb;
import greendao.BookDbDao;
import greendao.DaoMaster;
import greendao.DaoSession;
import greendao.RawBookDb;
import greendao.RawBookDbDao;
import pl.wikibuster.bookcrossing.BaseActivity;
import pl.wikibuster.bookcrossing.FullBook;
import pl.wikibuster.bookcrossing.Message;
import pl.wikibuster.bookcrossing.R;
import pl.wikibuster.bookcrossing.network.model.Book;
import pl.wikibuster.bookcrossing.network.model.BookUser;
import pl.wikibuster.bookcrossing.network.model.RawBook;
import pl.wikibuster.bookcrossing.network.request.AddBookRequest;
import pl.wikibuster.bookcrossing.network.request.BookUserRequest;
import pl.wikibuster.bookcrossing.network.request.SearchIsbnRequest;
import pl.wikibuster.bookcrossing.network.request.UsersBooksRequest;

public class Library extends BaseActivity {

    public static final String PREFS_MODIFICATION_DATE = "last_modification_date";
    public static final String MODIFICATION_DATE = "modification_date";
    public static final String TAG = "Library";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LibraryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        displayBooks();

//        String token = getSharedPreferences(LoginActivity.PREF_TOKEN, Context.MODE_PRIVATE).getString(LoginActivity.TOKEN, "");
        BookUserRequest bookUserRequest = new BookUserRequest(null);
        getSpiceManager().execute(bookUserRequest, new BookUserListener());

    }

    private void downloadBooks(final BookUser bookUser) {
        UsersBooksRequest booksRequest = new UsersBooksRequest(bookUser);
        getSpiceManager().execute(booksRequest, new RequestListener<FullBook[]>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.w(TAG, "API communication failed", spiceException);
            }

            @Override
            public void onRequestSuccess(FullBook[] fullBooks) {
                List<FullBook> books = Arrays.asList(fullBooks);
                DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(Library.this, getString(R.string.databaseName), null);
                SQLiteDatabase db = helper.getWritableDatabase();
                DaoMaster daoMaster = new DaoMaster(db);
                DaoSession daoSession = daoMaster.newSession();
                RawBookDbDao rawBookDbDao = daoSession.getRawBookDbDao();
                List<RawBookDb> rawBooksDb = FullBook.getRawBooksDb(fullBooks);
                List<BookDb> booksDb = FullBook.getBooksDb(fullBooks);
                rawBookDbDao.insertInTx(rawBooksDb);

                for (int i = 0; i < rawBooksDb.size(); i++) {
                    booksDb.get(i).setRawBookDb(rawBooksDb.get(i));
                }

                BookDbDao bookDbDao = daoSession.getBookDbDao();
                bookDbDao.insertInTx(booksDb);

                adapter = new LibraryAdapter(books, getSpiceManager());
                recyclerView.setAdapter(adapter);
            }
        });
    }

    private void displayBooks() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(Library.this, getString(R.string.databaseName), null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        BookDbDao bookDbDao = daoSession.getBookDbDao();
        try {
            List<BookDb> data = bookDbDao.queryBuilder().list();
            adapter = new LibraryAdapter(FullBook.of(data), getSpiceManager());
            recyclerView.setAdapter(adapter);
        } catch (Exception e){
            LayoutInflater layoutInflater = LayoutInflater.from(Library.this);
            View view = layoutInflater.inflate(R.layout.nothing_to_show, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Library.this);
            alertDialogBuilder.setView(view);
            alertDialogBuilder.setNeutralButton((getString(R.string.goBack)),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
    }


    private class BookUserListener implements RequestListener<BookUser> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.w(TAG, "API communication failed", spiceException);
            Message.message(Library.this, (getString(R.string.wrongISBN)));
        }

        @Override
        public void onRequestSuccess(BookUser bookUser) {
            try {
                long externalTime = BookUser.DATE_FORMAT.parse(bookUser.getLastModification()).getTime();
                SharedPreferences sharedPreferences = Library.this.getSharedPreferences(PREFS_MODIFICATION_DATE, Context.MODE_PRIVATE);
                long localTime = sharedPreferences.getLong(MODIFICATION_DATE, 0);
                Log.d(TAG, "localTime = " + localTime + ", externalTime = " + externalTime);
                if (localTime < externalTime) {
                    Log.i(TAG, "External time is newer, downloading books");
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putLong(MODIFICATION_DATE, externalTime);
                    editor.apply();
                    downloadBooks(bookUser);
                    Message.message(Library.this, getString(R.string.bookAdded));
                } else {
                    Log.d(TAG, "No need to download new books");
                }
            } catch (ParseException e) {
                Log.e(TAG, "could not parse " + bookUser.getLastModification(), e);
            }

        }
    }

    public void scan(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setPrompt((getString(R.string.scan)));
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    public void input(View view) {
        LayoutInflater layoutInflater = LayoutInflater.from(Library.this);
        View promptView = layoutInflater.inflate(R.layout.add_book_input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Library.this);

        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);


        //editText.setRawInputType(Configuration.KEYBOARD_12KEY);
        alertDialogBuilder.setCancelable(true)
                .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String input = editText.getText().toString();
                        if(StringUtils.isNumeric(input) && input.length() == 10 || input.length() == 13){
                            Log.i("ISBN", input);
                            downloadInfo(input);
                        }else{
                            Log.i("ISBN", "bad format!");
                            Message.message(Library.this, getString(R.string.wrongISBN));
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            if(scanResult.getContents() != null && StringUtils.isNumeric(scanResult.getContents())) {
                String isbn = scanResult.getContents();
                Log.i("ISBN", isbn);
                downloadInfo(isbn);
            }
        }
    }

    private void downloadInfo(String isbn) {
        SearchIsbnRequest request = new SearchIsbnRequest(isbn);
        getSpiceManager().execute(request, new SearchIsbnListener());
    }

    public final class SearchIsbnListener implements RequestListener<RawBook> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e("ISBN", "Rest API communication failed");

        }

        @Override
        public void onRequestSuccess(final RawBook rawBook) {
            String title = rawBook.getTitle();
            String author = StringUtils.join(rawBook.getAuthors(), ", ");
            Log.i("ISBN", "Rest API succeeded. Book = " + title + " (" + author + ")");

            StringBuilder builder = new StringBuilder();
            builder.append(getString(R.string.title)).append(title).append("\n");
            builder.append(getString(R.string.author)).append(author);

            LayoutInflater layoutInflater = LayoutInflater.from(Library.this);
            View view = layoutInflater.inflate(R.layout.accept_book_dialog, null);
            TextView textElement = (TextView)view.findViewById(R.id.textView);
            textElement.setText(builder);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Library.this);
            alertDialogBuilder.setView(view);
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton(getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    AddBookRequest request = new AddBookRequest(rawBook.getId());
                                    getSpiceManager().execute(request, new AddBookListener(rawBook));
                                    dialog.cancel();
                                }
                            })
                    .setNegativeButton(getString(R.string.no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
    }

    private class AddBookListener implements RequestListener<Book> {

        private final RawBook rawBook;

        public AddBookListener(RawBook rawBook) {
            this.rawBook = rawBook;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.e(TAG, "Could not add book", spiceException);
        }

        @Override
        public void onRequestSuccess(Book book) {

            RawBookDb rawBookDb = rawBook.toDb();
            BookDb bookDb = book.toDb();

            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(getApplicationContext(), getString(R.string.databaseName), null);
            SQLiteDatabase db = helper.getWritableDatabase();
            DaoMaster daoMaster = new DaoMaster(db);
            DaoSession daoSession = daoMaster.newSession();
            daoSession.insert(rawBookDb);
            bookDb.setRawBookDb(rawBookDb);

            daoSession.insert(bookDb);

            SharedPreferences sharedPreferences = Library.this.getSharedPreferences(PREFS_MODIFICATION_DATE, Context.MODE_PRIVATE);
            sharedPreferences.edit().putLong(MODIFICATION_DATE, System.currentTimeMillis()).apply();

            FullBook fullBook  = new FullBook(bookDb, rawBookDb);
            adapter.add(fullBook);
            adapter.notifyDataSetChanged();
        }
    }
}

package pl.wikibuster.bookcrossing.network.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.RawBook;

/**
 * Created by kuba on 24.10.2015.
 */
public class SearchIsbnRequest extends RetrofitSpiceRequest<RawBook, BookCrossingApi> {

    private final String isbn;

    public SearchIsbnRequest(String isbn) {
        super(RawBook.class, BookCrossingApi.class);
        this.isbn = isbn;
    }

    @Override
    public RawBook loadDataFromNetwork() throws Exception {
        RawBook rawBook = getService().search(isbn);

        return rawBook;
    }

}

package pl.wikibuster.bookcrossing.network.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.BookUser;

/**
 * Created by kuba on 24.10.2015.
 */
public class BookUserRequest extends RetrofitSpiceRequest<BookUser, BookCrossingApi> {

    private final String token;

    public BookUserRequest(String token) {
        super(BookUser.class, BookCrossingApi.class);
        this.token = token;
    }

    @Override
    public BookUser loadDataFromNetwork() throws Exception {
        if (token == null || token.isEmpty() || token.equals("empty")) {
            return getService().getUser();
        } else {
            return getService().getUser(token);
        }
    }
}

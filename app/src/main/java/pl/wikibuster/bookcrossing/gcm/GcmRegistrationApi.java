package pl.wikibuster.bookcrossing.gcm;

import retrofit.ResponseCallback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kuba on 25.10.2015.
 */
public interface GcmRegistrationApi {

    @FormUrlEncoded()
    @POST("/register/")
    ResponseCallback register(@Field("registration_id") String registrationId, @Field("android_id") String androidId, @Field("token") String token);

}

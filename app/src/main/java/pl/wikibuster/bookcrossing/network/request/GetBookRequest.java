package pl.wikibuster.bookcrossing.network.request;

/**
 * Created by michal on 25.10.15.
 */

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;



import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;

/**
 * Created by michal on 25.10.2015.
 */
public class GetBookRequest extends RetrofitSpiceRequest<Book, BookCrossingApi> {

    private final long bookId;

    public GetBookRequest(long bookId) {
        super(Book.class, BookCrossingApi.class);
        this.bookId = bookId;
    }

    @Override
    public Book loadDataFromNetwork() throws Exception {
        return getService().getBook(bookId);
    }
}

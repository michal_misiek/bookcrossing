package pl.wikibuster.bookcrossing;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import greendao.DaoMaster;
import greendao.DaoSession;
import pl.wikibuster.bookcrossing.gcm.RegistrationIntentService;
import pl.wikibuster.bookcrossing.library.Library;
import pl.wikibuster.bookcrossing.login.LoginActivity;

public class MainActivity extends BaseActivity {


    public static final String PREF_TOKEN = "pref_token";
    public static final String TOKEN = "token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pl.wikibuster.bookcrossing.R.layout.activity_main);

        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }

    public void searchBook(View view) {
        Intent intent = new Intent(this, SearchBook.class);
        startActivity(intent);
    }

    public void library(View view) {
        Intent intent = new Intent(this, Library.class);
        startActivity(intent);
    }

    public void borrowed(View view) {
        Intent intent = new Intent(this, Borrowed.class);
        startActivity(intent);
    }

    public void history(View view) {
        Intent intent = new Intent(this, History.class);
        startActivity(intent);
    }

    public void logout(View view) {

        SharedPreferences preferences = getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE);
        preferences.edit().putString(TOKEN, "empty").apply();
        SharedPreferences sharedPreferences = getSharedPreferences(Library.PREFS_MODIFICATION_DATE, Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong(Library.MODIFICATION_DATE, 0).apply();

        //String token = getSharedPreferences(PREF_TOKEN, Context.MODE_PRIVATE).getString(TOKEN, "");

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(MainActivity.this, getString(R.string.databaseName), null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        daoSession.getBookDbDao().deleteAll();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    public void aboutUser(View view) {
        Intent intent = new Intent(this, AboutUser.class);
        startActivity(intent);
    }
}

package pl.wikibuster.bookcrossing.network.request;

import android.util.Log;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.LoginUser;
import pl.wikibuster.bookcrossing.network.model.Token;

/**
 * Created by kuba on 24.10.2015.
 */
public class AuthenticationRequest extends RetrofitSpiceRequest<Token, BookCrossingApi> {

    private final String username;
    private final String password;

    public AuthenticationRequest(String username, String password) {
        super(Token.class, BookCrossingApi.class);
        this.username = username;
        this.password = password;
    }


    @Override
    public Token loadDataFromNetwork() throws Exception {
        Log.d("Login", "authenticating with " + username + " " + password);
        LoginUser user = new LoginUser(username, password);
        return getService().authenticate(user);
    }
}

package pl.wikibuster.bookcrossing;

import android.app.Activity;

import com.octo.android.robospice.SpiceManager;

import pl.wikibuster.bookcrossing.network.RetrofitSpiceService;

/**
 * Created by kuba on 24.10.2015.
 */
public class BaseActivity extends Activity {

    private SpiceManager spiceManager = new SpiceManager(RetrofitSpiceService.class);

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }

}

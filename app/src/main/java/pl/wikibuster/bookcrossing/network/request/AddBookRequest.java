package pl.wikibuster.bookcrossing.network.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;

/**
 * Created by kuba on 24.10.2015.
 */
public class AddBookRequest extends RetrofitSpiceRequest<Book, BookCrossingApi> {

    private final long rawBookId;

    public AddBookRequest(long rawBookId) {
        super(Book.class, BookCrossingApi.class);
        this.rawBookId = rawBookId;
    }

    @Override
    public Book loadDataFromNetwork() throws Exception {
        return getService().addBook(rawBookId);
    }
}

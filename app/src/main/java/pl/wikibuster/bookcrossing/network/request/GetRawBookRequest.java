package pl.wikibuster.bookcrossing.network.request;

/**
 * Created by michal on 25.10.15.
 */

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;



import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;
import pl.wikibuster.bookcrossing.network.model.RawBook;

/**
 * Created by michal on 25.10.2015.
 */
public class GetRawBookRequest extends RetrofitSpiceRequest<RawBook, BookCrossingApi> {

    private final long bookId;

    public GetRawBookRequest(long bookId) {
        super(RawBook.class, BookCrossingApi.class);
        this.bookId = bookId;
    }

    @Override
    public RawBook loadDataFromNetwork() throws Exception {
        return getService().getRawBook(bookId);
    }
}

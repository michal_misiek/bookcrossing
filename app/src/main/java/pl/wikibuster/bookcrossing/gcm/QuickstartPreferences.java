package pl.wikibuster.bookcrossing.gcm;

/**
 * Created by kuba on 25.10.2015.
 */
public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}
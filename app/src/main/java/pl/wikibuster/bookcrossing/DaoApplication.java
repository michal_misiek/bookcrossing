package pl.wikibuster.bookcrossing;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import greendao.BookDbDao;
import greendao.DaoMaster;
import greendao.RawBookDbDao;

/**
 * Created by michal on 24.10.15.
 */
public class DaoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, getString(R.string.databaseName), null);
        SQLiteDatabase db = helper.getWritableDatabase();
        RawBookDbDao.createTable(db, true);
        BookDbDao.createTable(db, true);
    }
}

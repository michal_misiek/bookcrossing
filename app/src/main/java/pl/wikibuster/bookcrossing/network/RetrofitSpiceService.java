package pl.wikibuster.bookcrossing.network;

import android.content.Context;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import pl.wikibuster.bookcrossing.login.LoginActivity;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by kuba on 24.10.2015.
 */
public class RetrofitSpiceService extends RetrofitGsonSpiceService {

    private static final String BASE_URL = "http://wikibuster.pl/api";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(BookCrossingApi.class);
    }

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        return super.createRestAdapterBuilder().setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                String token = getApplicationContext().getSharedPreferences(LoginActivity.PREF_TOKEN,
                        Context.MODE_PRIVATE).getString(LoginActivity.TOKEN, "");
                request.addHeader("Authorization", "Token " + token);
            }
        });
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }
}

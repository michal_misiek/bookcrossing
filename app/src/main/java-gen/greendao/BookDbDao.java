package greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.internal.SqlUtils;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table BOOK_DB.
*/
public class BookDbDao extends AbstractDao<BookDb, Long> {

    public static final String TABLENAME = "BOOK_DB";

    /**
     * Properties of entity BookDb.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property ServerId = new Property(1, Long.class, "serverId", false, "SERVER_ID");
        public final static Property RawBookId = new Property(2, Long.class, "rawBookId", false, "RAW_BOOK_ID");
        public final static Property ServerOwnerId = new Property(3, Long.class, "serverOwnerId", false, "SERVER_OWNER_ID");
        public final static Property Rating = new Property(4, Float.class, "rating", false, "RATING");
        public final static Property BorrowedBy = new Property(5, Long.class, "borrowedBy", false, "BORROWED_BY");
        public final static Property Location = new Property(6, String.class, "location", false, "LOCATION");
        public final static Property Description = new Property(7, String.class, "description", false, "DESCRIPTION");
    }

    private DaoSession daoSession;


    public BookDbDao(DaoConfig config) {
        super(config);
    }
    
    public BookDbDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'BOOK_DB' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'SERVER_ID' INTEGER," + // 1: serverId
                "'RAW_BOOK_ID' INTEGER," + // 2: rawBookId
                "'SERVER_OWNER_ID' INTEGER," + // 3: serverOwnerId
                "'RATING' REAL," + // 4: rating
                "'BORROWED_BY' INTEGER," + // 5: borrowedBy
                "'LOCATION' TEXT," + // 6: location
                "'DESCRIPTION' TEXT);"); // 7: description
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'BOOK_DB'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, BookDb entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Long serverId = entity.getServerId();
        if (serverId != null) {
            stmt.bindLong(2, serverId);
        }
 
        Long rawBookId = entity.getRawBookId();
        if (rawBookId != null) {
            stmt.bindLong(3, rawBookId);
        }
 
        Long serverOwnerId = entity.getServerOwnerId();
        if (serverOwnerId != null) {
            stmt.bindLong(4, serverOwnerId);
        }
 
        Float rating = entity.getRating();
        if (rating != null) {
            stmt.bindDouble(5, rating);
        }
 
        Long borrowedBy = entity.getBorrowedBy();
        if (borrowedBy != null) {
            stmt.bindLong(6, borrowedBy);
        }
 
        String location = entity.getLocation();
        if (location != null) {
            stmt.bindString(7, location);
        }
 
        String description = entity.getDescription();
        if (description != null) {
            stmt.bindString(8, description);
        }
    }

    @Override
    protected void attachEntity(BookDb entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public BookDb readEntity(Cursor cursor, int offset) {
        BookDb entity = new BookDb( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // serverId
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // rawBookId
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3), // serverOwnerId
            cursor.isNull(offset + 4) ? null : cursor.getFloat(offset + 4), // rating
            cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5), // borrowedBy
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // location
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7) // description
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, BookDb entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setServerId(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setRawBookId(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setServerOwnerId(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
        entity.setRating(cursor.isNull(offset + 4) ? null : cursor.getFloat(offset + 4));
        entity.setBorrowedBy(cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5));
        entity.setLocation(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setDescription(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(BookDb entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(BookDb entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getRawBookDbDao().getAllColumns());
            builder.append(" FROM BOOK_DB T");
            builder.append(" LEFT JOIN RAW_BOOK_DB T0 ON T.'RAW_BOOK_ID'=T0.'_id'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected BookDb loadCurrentDeep(Cursor cursor, boolean lock) {
        BookDb entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        RawBookDb rawBookDb = loadCurrentOther(daoSession.getRawBookDbDao(), cursor, offset);
        entity.setRawBookDb(rawBookDb);

        return entity;    
    }

    public BookDb loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<BookDb> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<BookDb> list = new ArrayList<>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<BookDb> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<BookDb> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}

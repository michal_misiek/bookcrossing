package pl.wikibuster.bookcrossing;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Olo on 2015-10-25.
 */
public class AboutUser extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutuser);
    }

    public void change(View view){
        Message.message(AboutUser.this, getString(R.string.change));
    }
}



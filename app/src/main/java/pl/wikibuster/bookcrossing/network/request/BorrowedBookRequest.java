package pl.wikibuster.bookcrossing.network.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.List;

import greendao.BookDb;
import greendao.RawBookDb;
import pl.wikibuster.bookcrossing.FullBook;
import pl.wikibuster.bookcrossing.network.BookCrossingApi;
import pl.wikibuster.bookcrossing.network.model.Book;
import pl.wikibuster.bookcrossing.network.model.BookUser;
import pl.wikibuster.bookcrossing.network.model.PagingWrapper;
import pl.wikibuster.bookcrossing.network.model.RawBook;

/**
 * Created by kuba on 24.10.2015.
 */
public class BorrowedBookRequest extends RetrofitSpiceRequest<FullBook[], BookCrossingApi> {


    private final BookUser bookUser;

    public BorrowedBookRequest(BookUser bookUser) {
        super(FullBook[].class, BookCrossingApi.class);
        this.bookUser = bookUser;
    }

    @Override
    public FullBook[] loadDataFromNetwork() throws Exception {
        PagingWrapper<Book> result = getService().getBooksBorrowed(bookUser.getId());
        List<Book> books = result.getResults();
        int i = 2;
        while (result.getNext() != null) {
            result = getService().getBooksBorrowed(bookUser.getId(), i);
            books.addAll(result.getResults());
            i++;
        }

        List<FullBook> list = new ArrayList<>();
        for (Book book : books) {
            RawBook rawBook = getService().getRawBook(book.getRaw_book());
            RawBookDb rawBookDb = rawBook.toDb();
            BookDb bookDb = book.toDb();
            bookDb.setRawBookDb(rawBookDb);

            list.add(new FullBook(bookDb, rawBookDb));
        }
        return list.toArray(new FullBook[list.size()]);
    }
}

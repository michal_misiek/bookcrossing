package pl.surecase.eu;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(4, "greendao");

        Entity rawBook = schema.addEntity("RawBookDb");
        rawBook.addIdProperty();
        rawBook.addLongProperty("serverId");
        rawBook.addStringProperty("isbn10");
        rawBook.addStringProperty("isbn13");
        rawBook.addStringProperty("title");
        rawBook.addStringProperty("author");
        rawBook.addStringProperty("publisher");
        rawBook.addIntProperty("year");
        rawBook.addStringProperty("genre");
        rawBook.addStringProperty("thumbnail");
        rawBook.addStringProperty("language");
        rawBook.addFloatProperty("rating");

        Entity book = schema.addEntity("BookDb");
        book.addIdProperty();
        book.addLongProperty("serverId");
        Property rawBookId = book.addLongProperty("rawBookId").getProperty();
        book.addToOne(rawBook, rawBookId);
        book.addLongProperty("serverOwnerId");
        book.addFloatProperty("rating");
        book.addLongProperty("borrowedBy");
        book.addStringProperty("location");
        book.addStringProperty("description");

        new DaoGenerator().generateAll(schema, args[0]);
    }
}

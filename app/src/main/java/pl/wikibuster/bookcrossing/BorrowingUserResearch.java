package pl.wikibuster.bookcrossing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.RecursiveAction;

import pl.wikibuster.bookcrossing.library.LibraryAdapter;
import pl.wikibuster.bookcrossing.network.model.Book;
import pl.wikibuster.bookcrossing.network.model.BookUser;
import pl.wikibuster.bookcrossing.network.model.RawBook;
import pl.wikibuster.bookcrossing.network.model.User;
import pl.wikibuster.bookcrossing.network.request.BorrowingUserRequest;
import pl.wikibuster.bookcrossing.network.request.GetBookRequest;
import pl.wikibuster.bookcrossing.network.request.GetRawBookRequest;

/**
 * Created by michal on 25.10.15.
 */
public class BorrowingUserResearch extends BaseActivity {

    public static final String TAG = "Library";
    public static final String ID = "id";
    public static final String BOOK_ID = "book_id";

    TextView userTextView = (TextView) findViewById(R.id.user);
    TextView bookTextView = (TextView) findViewById(R.id.book);

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private LibraryAdapter adapter;


    private Book myBook;
    private BookUser borrowingUser;
    private RawBook rawBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrowing_user_research);

        Intent intent = getIntent();
        long id = intent.getLongExtra(ID, 0);
        long bookId = intent.getLongExtra(BOOK_ID, 0);


        BorrowingUserRequest borrowingBookRequest = new BorrowingUserRequest(id);
        getSpiceManager().execute(borrowingBookRequest, new RequestListener<BookUser>()
        {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.w(TAG, "API communication failed", spiceException);
            }

            @Override
            public void onRequestSuccess(BookUser user) {
                borrowingUser = user;
                if (myBook != null) {
                    run();
                }
            }
        });

        GetBookRequest getBookRequest = new GetBookRequest(bookId);
        getSpiceManager().execute(getBookRequest, new RequestListener<Book>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.w(TAG, "API communication failed", spiceException);
            }

            @Override
            public void onRequestSuccess(Book book) {
                myBook = book;
                getRawBook(book.getRaw_book());
                if (borrowingUser != null) {
                    run();
                }
            }
        });


    }

    public void getRawBook(long id){
        GetRawBookRequest getRawBookRequest = new GetRawBookRequest(id);
        getSpiceManager().execute(getRawBookRequest, new RequestListener<RawBook>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.w(TAG, "API communication failed", spiceException);
            }

            @Override
            public void onRequestSuccess(RawBook book) {
                rawBook = book;

            }
        });
    }

    private void run() {

        bookTextView.setText(rawBook.getTitle() + " " + rawBook.getAuthors());
        userTextView.setText(borrowingUser.getFullName());
        //adapter = new LibraryAdapter(myBook);
        //recyclerView.setAdapter(adapter);
        input();

    }


    public void input() {
        LayoutInflater layoutInflater = LayoutInflater.from(BorrowingUserResearch.this);
        View promptView = layoutInflater.inflate(R.layout.add_book_input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BorrowingUserResearch.this);

        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);


        //editText.setRawInputType(Configuration.KEYBOARD_12KEY);
        alertDialogBuilder.setCancelable(true)
                .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String input = editText.getText().toString();
                        if(StringUtils.isNumeric(input) && input.length() == 10 || input.length() == 13){
                            Log.i("ISBN", input);
                            //downloadInfo(input);
                        }else{
                            Log.i("ISBN", "bad format!");
                            Message.message(BorrowingUserResearch.this, getString(R.string.wrongISBN));
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

}